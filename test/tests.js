const sinon = require('sinon');
const assert = require('assert');
const expect = require('chai').expect;

class Pokemon {
  constructor(name, level) {
    this.name = name;
    this.level = level;
  }

  show() {
    console.log(`Hi! My name is ${this.name}, my level is ${this.level}`);
  }

  valueOf() {
    return this.level;
  }
}


class Pokemonlist extends Array {
  constructor(...items) {
    items = items.filter(
      item => item instanceof Pokemon
    );

    super(...items);
  }

  add(name, level) {
    let newPokemon = new Pokemon(name, level);

    this.push(newPokemon);
  }

  show() {
    this.forEach(function(item) {
      item.show();
    });

    console.log(`There are ${this.length} pokemons here.`);
  }

  max() {
    let strongestPokemon = Math.max(...this);

    return this.find(
      item => item.level == strongestPokemon
    );
  }
}

describe('Pokemon', () => {
	describe('.show()', () => {
		afterEach(() => {
			console.log.restore();
		});

		it('Две строки вернут текст с описанием покемона', () => {
			let spy = sinon.spy(console, 'log');
			let pokemon = new Pokemon('Peekachoo', '100');

			pokemon.show();

			assert(spy.calledWith('Hi! My name is Peekachoo, my level is 100'));
		});
	
		it('Строка и число вернут текст с описанием покемона', () => {
			let spy = sinon.spy(console, 'log');
			let pokemon = new Pokemon('Peekachoo', 100);

			pokemon.show();

			assert(spy.calledWith('Hi! My name is Peekachoo, my level is 100'));
		});
	
		it('Отсутствие аргументов вернет описание с undefined вместо имени и уровня', () => {
			let spy = sinon.spy(console, 'log');
			let pokemon = new Pokemon();

			pokemon.show();

			assert(spy.calledWith('Hi! My name is undefined, my level is undefined'));
		});
	
		it('Аргументы undefined вернут описание с undefined', () => {
			let spy = sinon.spy(console, 'log');
			let pokemon = new Pokemon(undefined, undefined);

			pokemon.show();

			assert(spy.calledWith('Hi! My name is undefined, my level is undefined'));
		});
	
		it('Аргументы null вернут описание с null', () => {
			let spy = sinon.spy(console, 'log');
			let pokemon = new Pokemon(null, null);

			pokemon.show();

			assert(spy.calledWith('Hi! My name is null, my level is null'));
		});
	
		it('Аргументы NaN вернут описание с NaN', () => {
			let spy = sinon.spy(console, 'log');
			let pokemon = new Pokemon(NaN, NaN);

			pokemon.show();

			assert(spy.calledWith('Hi! My name is NaN, my level is NaN'));
		});
	});
});

describe('Pokemonlist', () => {
	describe('.add()', () => {
		it('Ничего не возвращает', () => {
			let pokemonList = new Pokemonlist();

			const result = pokemonList.add('Peekachoo', 100);
			
			expect(result).to.equal(undefined);
		});

		it('Длина массива увеличивается на 1', () => {
			let pokemonList = new Pokemonlist();

			const lengthBefore = pokemonList.length;
			
			pokemonList.add('Peekachoo', 100);

			const lengthAfter = pokemonList.length;
			
			expect(lengthAfter - lengthBefore).to.equal(1);
		});

		it('В массив записывается объект', () => {
			let pokemonList = new Pokemonlist();

			pokemonList.add('Peekachoo', 100);

			const result = pokemonList[pokemonList.length - 1];

			expect(result).to.be.an('object');
		});

		it('Запись данных в массив класса Pokemonlist происходит успешно', () => {
			let pokemonList = new Pokemonlist();

			pokemonList.add('Peekachoo', 100);

			const result = pokemonList[pokemonList.length - 1];

			expect(result).to.eql({
				name: 'Peekachoo',
				level: 100
			});
		});
	});

	describe('.show()', () => {
		let pokemonList = new Pokemonlist();

		beforeEach(() => {
			pokemonList.add('Peekachoo', 100);
		});

		it('Отображает корректное количество покемонов в массиве', () => {
			let spy = sinon.spy(console, 'log');

			pokemonList.show();

			assert(spy.calledWith('There are 1 pokemons here.'));
		});
	});

	describe('.max()', () => {
		const tests = [{
			args: [{
				name: '100 Integer',
				level: 100
			}, {
				name: '3000 Integer',
				level: 3000
			}],
			expected: {
				name: '3000 Integer',
				level: 3000
			}
		}, {
			args: [{
				name: '100 Integer',
				level: 100
			}, {
				name: '-5000 Integer',
				level: -5000
			}, {
				name: '0 Integer',
				level: 0
			}],
			expected: {
				name: '100 Integer',
				level: 100
			}
		}, {
			args: [{
				name: '100 Integer',
				level: 100
			}, {
				name: '-5000 Integer',
				level: -5000
			}, {
				name: '-100 Integer',
				level: -100
			}, {
				name: '-200 Integer',
				level: -200
			}],
			expected: {
				name: '100 Integer',
				level: 100
			}
		}];

		tests.forEach(test => {
			it(`работает корректно с ${test.args.length} аргументами`, () => {
				let pokemonList = new Pokemonlist();

				test.args.forEach(sample => {
					pokemonList.add(sample.name, sample.level);
				});

				expect(pokemonList.max()).to.eql(test.expected);
			});
		});

		it('возвращает undefined, если Pokemonlist пуст', () => {
			let pokemonList = new Pokemonlist();

			expect(pokemonList.max()).to.equal(undefined);
		});

		it('работает корректно и с string значениями level', () => {
			let pokemonList = new Pokemonlist();

			pokemonList.add('Peekachoo', 100);
			pokemonList.add('Super Peekachoo with string value of level', '10000');

			expect(pokemonList.max()).to.eql({
				name: 'Super Peekachoo with string value of level',
				level: '10000'
			});
		});
	});
});